/**
 * @todo convert into a jQuery plugin
 */

var ghAPI = {
	
	/*
	 * Available {{Details}}
	 */
	placeHolders : [
	                'author__name','author__login','author__email','authored_date',
	                'committer__name','committer__login','committer__email','committed_date',
	                'id','message','url'
	                ],
	
	/*
	 * Used if template is not specified in node template
	 * Custom template should be place inside a container with class .github-feed-template
	 */
	defaultTemplate : '<p><strong>{{committer__name}}:</strong> <a href="{{url}}" target="_blank">{{message}}</a></p>' ,
	
	
	/*
	 * JSONp call to get GitHub feed 
	 * @todo refactor into smaller functions
	 */
	displayCommits : function( user, repo, branch, container, limit ){
		
		// Repository URL
		var settings = Drupal.settings.github_feed;
		var url = settings.base_url + settings.commit_list_path
			.replace('[username]',user)
			.replace('[repository]',repo)
			.replace('[branch]',branch);
		
		// Number of entries to display
		var limit = limit || false;
		
		// jQuery JSONp call
		jQuery.ajax({
			"dataType" : 'jsonp',
			"url" : url,
			"success" : function(data){
				
				// Get custom template
				var customTemplate = jQuery(container).parent().children('.github-feed-template');
				var template = false;
				
				if( customTemplate.length > 0 ) {
					template = customTemplate.html();
				}else{
					template = ghAPI.defaultTemplate;				
				}
				
				// Change back any URL encoded mustaches
				template = template.replace(/%7B%7B/g,"{{");
				template = template.replace(/%7D%7D/g,"}}");
				
				// Set base URL on any links to GitHub
				template = template.replace(/{{url}}/g, Drupal.settings.github_feed.base_url + "/{{url}}");
				
				var htmlArray = [];
				
				for( var i = 0; i < data.commits.length && ( i < limit || limit == false ); i++ )
				{
					// Get current row from JSON response
					var item = data.commits[i];
					
					// Reset HTML for each entry
					var itemHtml = template;
					
					for( var p = 0; p < ghAPI.placeHolders.length; p++ )
					{
						// Split parts on double underscore - e.g. author__name
						var parts = ghAPI.placeHolders[p].split('__');
						
						// Get base value - e.g. author (obj), url (string)
						var placeholder = item[parts[0]];
						
						if( typeof(placeholder) != 'undefined' ) {
							
							// Get sub value e.g. author->name
							if( typeof(parts[1]) != 'undefined' ) {
								placeholder = placeholder[parts[1]];
							}
							
							// Replace all occurences of each placeholder with their value
							var pattern = "{{" + ghAPI.placeHolders[p] + "}}";
							var regex = new RegExp(pattern,'g');
							itemHtml = itemHtml.replace(regex,placeholder);
						}
					}
					
					// Push onto array of entries
					htmlArray.push(itemHtml);
				}
				
				// Load result into container
				jQuery(container).html(htmlArray.join(''));
			}
		});
	}
		
}